
import pandas as pd
train = pd.read_csv("tweet_train.tsv", header=0, \
delimiter="\t", quoting=3)
train.shape
train.columns.values


print("tokenization des tweets...")
from analyse_outils import tweet_to_words
num_tweets = len(train["tweet"])
clean_train_tweets = []
for i in range(num_tweets):
    clean_tweet = tweet_to_words(train["tweet"][i])
    clean_train_tweets.append(clean_tweet)

# extraction des caracteristique
print("Extraction des caracteristique ...")
from sklearn.feature_extraction.text \
     import CountVectorizer
vectorizer = CountVectorizer(
    analyzer = "word",
    max_features = 5000
     )
train_data_features = vectorizer.fit_transform(
    clean_train_tweets
)
train_data_features = train_data_features.toarray()

print train_data_features.shape

#classification
print("Entrainement du classifieur naive bayesienne ...")
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
classifier = MultinomialNB()
classifier.fit(train_data_features, train["sentiment"])
print("test et apprentissage supervise  du classifieur naive bayesienne...")
test = pd.read_csv("tweet_test.tsv", header=0, \
                   delimiter="\t", quoting=3 )
num_tweets = len(test["tweet"])
clean_test_tweets = []
for i in range(num_tweets):
    clean_tweet = tweet_to_words(test["tweet"][i])
    clean_test_tweets.append(clean_tweet)
test_data_features = vectorizer.transform(clean_test_tweets)
test_data_features = test_data_features.toarray()

score = classifier.score(test_data_features,test["sentiment"])
print("Scort d'apprentissage ...")
print score


