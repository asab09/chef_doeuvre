import nltk

french_tok_file = 'tokenizers/punkt/french.pickle'
english_tok_file= 'tokenizers/punkt/english.pickle'
sent_tok = nltk.tokenize.load(english_tok_file)
word_tok = nltk.tokenize.TreebankWordTokenizer()

def tweet_to_words(raw_tweet):
    tweet_string = raw_tweet.decode('utf8')
    tweet_lower = tweet_string.lower()
    sents = sent_tok.tokenize(tweet_lower)
    tokens = []
    for s in sents:
        tokens.extend(word_tok.tokenize(s))
    return " ".join(tokens)
