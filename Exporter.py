# -*- coding: utf-8 -*-

import sys, getopt, got, datetime, codecs, json, csv, io, JsonParser

tweets_writen = 0
tweets_writen_file = 0
nb_file = 1


def main(argv):
    if len(argv) == 0:
        print 'You must pass some parameters. Use \"-h\" to help.'
        return

    if len(argv) == 1 and argv[0] == '-h':
        print """\nTo use this jar, you can pass the folowing attributes:
    username: Username of a specific twitter account (without @)
       since: The lower bound date (yyyy-mm-aa)
       until: The upper bound date (yyyy-mm-aa)
 querysearch: A query text to be matched
   maxtweets: The maximum number of tweets to retrieve

 \nExamples:
 # Example 1 - Get tweets by username [barackobama]
 python Exporter.py --username "barackobama" --maxtweets 1\n

 # Example 2 - Get tweets by query search [europe refugees]
 python Exporter.py --querysearch "europe refugees" --maxtweets 1\n

 # Example 3 - Get tweets by query search and bound dates [europe refugees]
 python Exporter.py --querysearch "donald trump lang:en" --since 2016-11-01 --until 2016-11-20 --maxtweets 1000\n

 # Example 4 - Get tweets by username and bound dates [barackobama, '2015-09-10', '2015-09-12']
 python Exporter.py --username "barackobama" --since 2015-09-10 --until 2015-09-12 --maxtweets 1\n
 
 # Example 5 - Get the last 10 top tweets by username
 python Exporter.py --username "barackobama" --maxtweets 10 --toptweets\n"""
        return

    try:
        opts, args = getopt.getopt(argv, "",
                                   ("username=", "since=", "until=", "querysearch=", "toptweets", "maxtweets="))

        tweetCriteria = got.manager.TweetCriteria()

        for opt, arg in opts:
            if opt == '--username':
                tweetCriteria.username = arg

            elif opt == '--since':
                tweetCriteria.since = arg

            elif opt == '--until':
                tweetCriteria.until = arg

            elif opt == '--querysearch':
                tweetCriteria.querySearch = arg

            elif opt == '--toptweets':
                tweetCriteria.topTweets = True

            elif opt == '--maxtweets':
                tweetCriteria.maxTweets = int(arg)
                maxTweets = int(arg)

        outputFile = codecs.open("./tweets-collected/" + str(nb_file) + "_output_got.json", "wb", "utf-8")

        print 'Searching...\n'
        outputFile.write('{"Tweets": [\n')
        outputFile.flush()
        outputFile.close()

        def receiveBuffer(tweets, f):
            global tweets_writen, tweets_writen_file, nb_file
            for t in tweets:
                f.write('{"username": "%s",' % t.username)
                f.write('"date": "%s",' % t.date.strftime("%Y-%m-%d %H:%M"))
                f.write('"retweets": %d,' % t.retweets)
                f.write('"favorites": %d,' % t.favorites)
                f.write('"replys": %d,' % t.replys)
                t.text = t.text.replace('"', ' ')
                f.write('"text": "%s",' % t.text)
                f.write('"geo": "%s",' % t.geo)
                f.write('"mentions": "%s",' % t.mentions)
                f.write('"hashtags": "%s",' % t.hashtags)
                f.write('"id": "%s",' % t.id)
                f.write('"permalink": "%s"}' % t.permalink)

                tweets_writen += 1

                tweets_writen_file += 1

                # on vérifie si le fichier ne dépasse pas la taille de tweets par fichier fixée (=1000)
                # si oui, on crée un autre fichier pour les autres tweets à récolter
                if tweets_writen_file == 1000:
                    # clotûre le fichier en cours pour éviter le débordement de taille
                    f.write('\n]\n}\n')
                    f.flush()
                    f.close()

                    nb_file += 1
                    tweets_writen_file = 0

                    # ouvre un nouveau fichier pour y ajouter le contenu restant
                    if tweets_writen < maxTweets:
                        f = codecs.open("./tweets-collected/" + str(nb_file) + "_output_got.json", "wb", "utf-8")
                    f.write('{"Tweets": [\n')
                elif tweets_writen < maxTweets and tweets_writen_file < 1000:
                    f.write(',\n')

            f.flush()
            print 'More %d saved on file...\n' % len(tweets)
            return f

        """ Partie du code qui appelle la fonction de récupération des tweets et cela de manière à couvrir
        toute la période spécifiée grâce au calcul de nombre de tweets à récolter par jour sur toute la période """
        nbDays = int(tweetCriteria.until[len(tweetCriteria.until) - 2:]) - int(
            tweetCriteria.since[len(tweetCriteria.since) - 2:]) + 1
        print "nbDays: ", nbDays
        tweetsPerDay = tweetCriteria.maxTweets / nbDays
        print "tweetsPerDay: ", tweetsPerDay
        tweetCriteria.maxTweets = tweetsPerDay

        # pour chaque jour, la récolte de N (tweetsPerDay) tweets est réalisée et ainsi de suite jusqu'à la date limite
        for d in range(1, nbDays + 1):
            if d < 10:
                tweetCriteria.since = tweetCriteria.since[:len(tweetCriteria.since) - 1] + str(d)
                tweetCriteria.until = tweetCriteria.since[:len(tweetCriteria.since) - 1] + str(d + 1)
            else:
                tweetCriteria.since = tweetCriteria.since[:len(tweetCriteria.since) - 2] + str(d)
                tweetCriteria.until = tweetCriteria.since[:len(tweetCriteria.since) - 2] + str(d + 1)

            got.manager.TweetManager.getTweets(tweetCriteria, nb_file, receiveBuffer)
            tweets_writen = 0

        outputFile = codecs.open("./tweets-collected/" + str(nb_file) + "_output_got.json", "ab", "utf-8")
        outputFile.write('\n]\n}\n')
        outputFile.flush()
        outputFile.close()
    except arg:
        print 'Arguments parser error, try -h' + arg


if __name__ == '__main__':
    main(sys.argv[1:])
