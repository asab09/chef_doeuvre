# coding=utf-8
import subprocess
from JsonParser import getAllUsername


def collectFollowingRelation():
    list_username = getAllUsername()
    for user in list_username[:10]:
        subprocess.Popen(["python", "StorageOnNeo4j", "init", user], stdout=subprocess.PIPE)


def getAllFollowing(user):
    list_following = []
    with  open(str(user) + ".dat", "r") as f_following:
        f_following.readline()
        for line in f_following:
            user = line.split(",")
            list_following.append((user[0], user[1]))
    return list_following


print "Collecte du réseau d'amis ... "
collectFollowingRelation()
print "OK\n"
