import glob
from pprint import pprint

import json
import pickle
import glob


def displayJson(filename="./output_got.json"):
    json_data = open(filename)
    print type(json_data), json_data

    data = json.load(json_data)
    print type(data)
    pprint(data)

    json_data.close()


def getAllUsername():
    list_username = []

    files = glob.glob("../tweets-collected/*.json")
    with open("list_username", "w") as f_username:
        pickler = pickle.Pickler(f_username)
        # parcours de tous les fichiers de collecte de tweets
        for f in files:
            json_data = open(f, "r")
            data = json.load(json_data)
            for tweets in data.iteritems():
                for t in tweets[1]:
                    if type(t) is dict:
                        pickler.dump(t["username"])
                        list_username.append(t["username"])
            json_data.close()
    return list_username


if __name__ == '__main__':
    list_username = getAllUsername()
