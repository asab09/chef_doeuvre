# coding=utf-8
import json

from py2neo import Graph
import glob
import JsonParser
import CollectFollowers



def storageUsersWithMentionsRelation():
    mdpNeo4j = "neo4jon"
    g = Graph("http://neo4j:" + mdpNeo4j + "@localhost:7474")
    files = glob.glob("../tweets-collected/*.json")

    # parcours de tous les fichiers de collecte de tweets
    for f in files:
        json_data = open(f, "r")
        data = json.load(json_data)
        for tweets in data.iteritems():
            for t in tweets[1]:
                if type(t) is dict:
                    score = t["retweets"] + t["favorites"] + t["replys"]
                    req = "MERGE (:Personne {username:\"" + t["username"] + "\", idTwitter:" + t[
                        "id"] + ", score:" + str(score) + "})"
                    g.run(req)
                    if t["mentions"] != "":
                        list_mention = str(t["mentions"]).replace(" ", "").split("@")
                        for u in list_mention:
                            if u != "":
                                req = "MERGE (a:Personne {username:'" + t[
                                    "username"] + "'}) MERGE (b:Personne {username:'" + u + "'}) MERGE (a)-[:MENTION]->(b)"
                                g.run(req)

                    json_data.close()
    print "OK\n"


def storageFollowingRelation():
    mdpNeo4j = "neo4jon"
    g = Graph("http://neo4j:" + mdpNeo4j + "@localhost:7474")
    list_username = JsonParser.getAllUsername()

    print "Stockage des réseaux d'amis ... "
    for user in list_username[:10]:
        list_following = CollectFollowers.getAllFollowing("./" + user)
        for user_followed in list_following:
            req = "MERGE (a:Personne {username:'" + user + "'}) MERGE (b:Personne {username:'" + user_followed[
                1] + "'}) MERGE (a)-[:FOLLOWING]->(b)"
            g.run(req)
    print "OK\n"


print "Stockage des tweetos et leur réseau de @mention ... "
storageUsersWithMentionsRelation()

print "Stockage du réseau d'amis ... "
storageFollowingRelation()
print "OK\n"
