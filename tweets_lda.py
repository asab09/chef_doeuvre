import logging
import os
import gensim
import codecs
import glob, logging, os, json, codecs, nltk, gensim, string

def getTopics():
	logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

	MODELS_DIR = "/home/aminesabri/Desktop/chef_doeuvre/mycorpus/"
	NUM_TOPICS = 15

	dictionary = gensim.corpora.Dictionary.load(os.path.join(MODELS_DIR, "mtsamples.dict"))
	corpus = gensim.corpora.MmCorpus(os.path.join(MODELS_DIR, "mtsamples.mm"))


	lda = gensim.models.LdaModel(corpus,  num_topics=NUM_TOPICS, id2word=dictionary, iterations=1000)
	
	l = lda.print_topics(NUM_TOPICS)
	return l

def distanceTweetTopic(tweet, topic):
	proba = 0
	for l in topic:
		l = l.strip()[l.rindex(":") + 2:]
	    	scores = [float(x.split("*")[0]) for x in l.split(" + ")]
	    	words = [x.split("*")[1] for x in l.split(" + ")]
		for word in words :
			if words not in tweet: 
	   			 continue
			else: proba += 1
	return proba
		


def classifyTweetsByTopic():
	l = getTopics()
	files = glob.glob("./tweets-collected/*.json")
	# parcours de tous les fichiers de collecte de tweets
	for f in files:
		json_data = open(f, "r")
		data = json.load(json_data)
		#topics = open("topics/topics.txt","rb")
		tweetClassified = codecs.open("topics/tweet_classified.json", "wb", "utf-8")
		for tweets in data.iteritems():
		    for t in tweets[1]:
			if type(t) is dict:
				i = 0
				for e in l: 
					if i == 0:
						best_d = distanceTweetTopic(t["text"], e[1])
						classe = e[1]
					else :
						d = distanceTweetTopic(t["text"], e[1])
						if d > best_d:
							best_d = d
							classe = e[1]
					i += 1
				t["classe"] = classe
				tweetClassified.write(t)
				tweetClassified.flush()
		tweetClassified.close()
	json_data.close()

classifyTweetsByTopic()


