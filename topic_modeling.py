import logging, os, json, codecs, nltk, gensim, string
from nltk.corpus import stopwords
from stop_words import get_stop_words
import glob

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


# Lire fichier json, selectionner le texte de chaque tweet, ecrire chaque tweet dans un fichier texte
tweets_writen = 0
tweets_writen_file = 0

files = glob.glob("./tweets-collected/*.json")
for f in files:
	tweets_writen_file = 0
	json_data = open(f, "r")
	data = json.load(json_data)
	list_tweet = []
	for tweets in data.iteritems():
	    for t in tweets[1]:
		if type(t) is dict:
		    tweets_writen += 1
		    output_file = codecs.open("/home/aminesabri/Desktop/chef_doeuvre/text_tweets/" + str(tweets_writen) + "_tweet_text.txt", 							"wb", "utf-8")
		    output_file.write(t["text"]+'\n')
		    tweets_writen_file += 1
		    output_file.close()
	if tweets_writen_file == 1000:
	    json_data.close()

"""
regex_str = [
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
]


tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)

def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if tokens_re.search(token) else token.lower() for token in tokens]
    return tokens

"""

def iter_docs(topdir, stoplist):
    for fn in os.listdir(topdir):
        fin = open(os.path.join(topdir, fn), 'rb')
        text = fin.read()
        fin.close()
        yield (x for x in 
	    gensim.utils.tokenize(text, lowercase=True, deacc=True, errors="ignore")
            if x not in stoplist)


class MyCorpus(object):

    def __init__(self, topdir, stoplist):
        self.topdir = topdir
        self.stoplist = stoplist
        self.dictionary = gensim.corpora.Dictionary(iter_docs(topdir, stoplist))
        
    def __iter__(self):
        for tokens in iter_docs(self.topdir, self.stoplist):
            yield self.dictionary.doc2bow(tokens)


TEXTS_DIR = "/home/aminesabri/Desktop/chef_doeuvre/text_tweets/"
MODELS_DIR = "/home/aminesabri/Desktop/chef_doeuvre/mycorpus/"

punctuation = list(string.punctuation)
st = [
	'via','trump', 'donald', 'com', 'www', 'Donald', 'https', 'http', 'YouTube', 'youtube','youtu', 'b', 'e', 'html', 'tweet', 		'Twitter', 'pic', 'TRUMP', 'DONALD', 'twitter', 'fb', 'tt', 'mg', 'elect', 'goo', 'gl', 'c', 'v', 'f', 'ln'
]

stoplist = stopwords.words('english') + punctuation + st
corpus = MyCorpus(TEXTS_DIR, stoplist)

corpus.dictionary.save(os.path.join(MODELS_DIR, "mtsamples.dict"))
gensim.corpora.MmCorpus.serialize(os.path.join(MODELS_DIR, "mtsamples.mm"), corpus)


