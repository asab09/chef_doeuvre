import logging
import os
import gensim
import pickle

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

MODELS_DIR = "/home/aminesabri/Desktop/chef_doeuvre/mycorpus/"
NUM_TOPICS = 15

dictionary = gensim.corpora.Dictionary.load(os.path.join(MODELS_DIR, "mtsamples.dict"))
corpus = gensim.corpora.MmCorpus(os.path.join(MODELS_DIR, "mtsamples.mm"))


lda = gensim.models.LdaModel(corpus,  num_topics=NUM_TOPICS, id2word=dictionary, iterations=1000)


f = open("topics/topics.txt","wb")
l = lda.print_topics(NUM_TOPICS)
for e in l: 
	f.write(str(e)+"\n")
f.close()

