Avant tout, le code est en langage Python 2, il faudra alors l'éxécuter dans un environnement Python 2 pour que cela fonctionne.
Chaque fonctionnalité de ce projet s'éxécute de manière dépendante de la première étape, à savoir la collecte des données, et doit suivre le schéma suivant :
(A savoir: toutes les données sont déjà collectées et prêtes, mais si vous voulez tester le code suivez les étapes)
    1 . Collecte des données et stockage des données:
        Se placer dans le dossier du projet et éxécuter la commande :
            - python Exporter.py --querysearch "Donald Trumpl lang:en" --since "2016-10-01" --until "2016-10-20" --maxtweets 20000
    2. Stockage des tweets et leur liens entre eux:
    (Avant de réaliser cette étape veillez à installer neo4j sur votre machine, et remplacer le texte des variables mdp par votre mdp neo4j
    dans les fonctions StorageUsersWithMentions() et StorageFollowingRelations() dans le fichier StorageGraphData.py)
        Se placer dans le dossier ./StorageOnNeo4j/ et éxécuter les commandes dans l'ordre:
            - python CollectFollowers.py
                Cette commande réalise la récupération du réseau d'amis pour chacun des tweetos récupérés
                Attention à attendre qu'il réalise entièrement la tâche, car il est effectué en subprocess, malgré que le terminal vous renvois une exécution finie,
                il faut savoir que cela n'est pas encore le cas.
                Ignorer les messages qui peuvent s'afficher, cela n'a aucun impact sur la tâche à réaliser
            - python StorageGraphData.py
                Cette commande permet de stocker tous les tweetos et leur liens entre eux (following et mentions)
    3. Récupération des thèmes les plus discutés :
        Cette partie se divise en deux étape : 
        - Construction du dictionnaire et le Bag-of-Words par l’exécution du fichier
         topic_modeling.py, les fichiers crées sont stockés dans le répertoire "mycorpus".
        - La 2ème étape consiste à appliquer le modèle LDA afin transformer les vecteurs 
         retournés par topic_modeling.py en topics, pour ce faire, il faut exécuter le  
         fichier lda_modeling.py qui crée un fichier texte contenant les topics les plus    
         discutés avec leurs probabilités d'apparition, ce fichier est stockés sur le
         répertoire "topics".
        - La 3ème étape, est la classification des tweets selon les thèmes retournés par
         LDA, malheureusement cette étape n’était pas accomplie, mais vous pouvez voir le
         code sur le fichier tweets_lda.py. 

    4. Analyse des sentiments
        Cette partie du projet est effectuée sur deux étapes :
        - La première étape consiste a la préparation des données d’entrées du
        classifieur (données d’entraînement et données de test, les tweets sont 
        triés sur les classes d'opinion négative et positive basé sur le trait des 
        émoticons) pour ce faire il faut exécuter le fichier count_emojis.py.
        - La deuxième étape consiste a l’entraînement et le test de notre 
        classifieur naive bayésienne avec les donnes produit de l’étape (1) pour ce 
        faire il faut exécuter le fichier analyse_sentiment_tweets.py