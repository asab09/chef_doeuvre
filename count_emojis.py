import logging, os, json, codecs, glob
from gensim import corpora
from nltk.tokenize import word_tokenize
from pprint import pprint
from collections import Counter
from nltk.corpus import stopwords
from nltk import bigrams
from collections import defaultdict
import re, operator, string

positive_vocab = [
    'good', 'nice', 'great', 'awesome', 'outstanding',
    'fantastic', 'terrific', ':)', ':-)', 'like', 'love',
     'triumph', 'triumphal', 'triumphant', 'victory',
'absolutely','adorable','accepted','acclaimed','accomplish','accomplishment','achievement',
'action','active','admire','adventure','affirmative','affluent','agree',
'agreeable','amazing','angelic','appealing','approve','aptitude','attractive','awesome',
'beaming','beautiful','believe','beneficial','bliss','bountiful','bounty',
'brave','bravo','brilliant',
'bubbly','calm','celebrated','certain','champ','champion','charming','cheery','choice',
'classic','classical','clean','commend','composed','congratulation','constant','cool',
'courageous','creative',
'cute','dazzling','delight','delightful','distinguished','divine','earnest',
'easy','ecstatic','effective','effervescent','efficient','effortless','electrifying',
'elegant','enchanting','encouraging','endorsed','energetic','energized',
'engaging','enthusiastic','essential','esteemed','ethical','excellent',
'exciting','exquisite','fabulous','fair','familiar','famous','fantastic',
'favorable','fetching','fine','fitting','flourishing','fortunate','free',
'fresh','friendly','fun','funny','generous','genius','genuine','giving',
'glamorous','glowing','good','gorgeous','graceful','great','green','grin',
'growing','handsome','happy','harmonious','healing','healthy','hearty','heavenly',
'honest','honorable','honored','hug','idea','ideal','imaginative','imagine',
'impressive','independent','innovate','innovative','instant','instantaneous','instinctive',
'intuitive','intellectual','intelligent','inventive','jovial','joy','jubilant','keen',
'kind','knowing','knowledgeable','laugh','legendary','light','learned',
'lively','lovely','lucid','lucky','luminous','marvelous','masterful','meaningful',
'merit','meritorious','miraculous','motivating','moving','natural','nice',
'novel','now','nurturing','nutritious','okay','one','onehundred','percent',
'open','optimistic','paradise','perfect','phenomenal','pleasurable','plentiful',
'pleasant','poised','polished','popular','positive','powerful','prepared',
'pretty','principled','productive','progress','prominent','protected','proud',
'quality','quick','quiet','ready','reassuring','refined','refreshing','rejoice',
'reliable','remarkable','resounding','respected','restored','reward','rewarding',
'right','robust','safe','satisfactory','secure','seemly','simple','skilled',
'skillful','smile','soulful','sparkling','special','spirited','spiritual','stirring',
'stupendous','stunning','success','successful','sunny','super','superb',
'supporting','surprising','terrific','thorough',
'thrilling','thriving','tops','tranquil','transforming',
'transformative','trusting','truthful','unreal','unwavering','up','upbeat','upright',
'upstanding','valued','vibrant','victorious','victory','vigorous','virtuous','vital',
'vivacious','wealthy','welcome','well','whole','wholesome','willing',
'wonderful','wondrous','worthy','wow','yes','yummy','zeal','zealous']
negative_vocab = [
    'bad', 'terrible', 'crap', 'useless', 'hate', ':(', ':-(',
    'defeat', 'sad','abysmal','adverse','alarming','angry','annoy','anxious','apathy','appalling',
'atrocious','awful','bad','banal','barbed','belligerent','bemoan','beneath','boring','broken',
'callous','clumsy','coarse','cold','cold-hearted','collapse','confused','contradictory','contrary',
'corrosive','corrupt','crazy','creepy','criminal','cruel','cry','cutting','dead','decaying','damage',
'damaging','dastardly','deplorable','depressed','','deformed','deny','despicable','detrimental','dirty',
'disease','disgusting','disheveled','dishonest','dishonorable','dismal','distress','dreadful','dreary',
'enraged','eroding','evil','fail','faulty','fear','feeble','fight','filthy','foul','frighten','frightful',
'gawky','ghastly','grave','greed','grim','grimace','gross','grotesque','gruesome','guilty','haggard','hard',
'hard-hearted','harmful','hate','hideous','homely','horrendous','horrible','hostile','hurt','hurtful','icky',
'ignore','ignorant','ill','immature','imperfect','impossible','inane','inelegant','infernal','injure','injurious',
'insane','insidious','insipid','jealous','junky','lose','lousy','lumpy','malicious','mean','menacing','messy','misshapen',
'missing','misunderstood','moan','moldy','monstrous','naive','nasty','naughty','negate','negative','never','no','nobody',
'nondescript','nonsense','noxious','objectionable','odious','offensive','old','oppressive','pain','perturb','pessimistic','petty',
'plain','poisonous','poor','prejudice','questionable','quirky','quit','reject','renege','repellant','reptilian','repulsive','repugnant',
'revenge','revolting','rocky','rotten','rude','ruthless','sad','savage','scare','scary','scream','severe','shoddy','shocking',
'sick','sickening','sinister','slimy','smelly','sobbing','sorry','spiteful','sticky','stinky','stormy','stressful','stuck',
'stupid','substandard','suspect','suspicious','tense','terrible','terrifying','threatening','ugly','undermine','unfair','unfavorable',
'unhappy','unhealthy','unjust','unlucky','unpleasant','upset','unsatisfactory','unsightly','untoward','unwanted',
'unwelcome','unwholesome','unwieldy','unwise','upset','vice','vicious','vile','villainous','vindictive','wary','weary','wicked',
'woeful','worthless','wound','yell','yucky','zero',]
#positive = re.compile(r'^'+positive_vocab+'$', re.VERBOSE | re.IGNORECASE)
#negative = re.compile(r'^'+positive_vocab+'$', re.VERBOSE | re.IGNORECASE)

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
    
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens

positif = 0
negatif = 0

list_tweet = []

files = glob.glob("./tweets-collected/*.json")
    # selector = NodeSelector(g)

    # parcours de tous les fichiers de collecte de tweets
for fi in files:
   json_data = open(fi, "r")
   data = json.load(json_data)
      
           
f = codecs.open('tweet_train.tsv', 'wb', 'utf-8')
f.write("idtweet"+"\t"+"sentiment"+"\t"+"tweet"+'\n')  
for tweets in data.iteritems():
    for t in tweets[1]:
    	negatif = 0
    	positif = 0
        if type(t) is dict:
	
           
	    terms_all = [term for term in preprocess(t['text'])]
	    for c in terms_all :
                
		if c in positive_vocab:
			positif=positif+1
		if c in negative_vocab:
			negatif=negatif+1
	    difference = positif - negatif
	    if (difference < 0 ) : 
	       f.write(t["id"]+"\t"+'0'+"\t"+t["text"]+'\n')
        
	    elif (difference > 0) :
	        f.write(t["id"]+"\t"+'1'+"\t"+t["text"]+'\n')
        
	

json_data.close()
f.close
